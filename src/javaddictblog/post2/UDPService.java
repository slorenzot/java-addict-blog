/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javaddictblog.post2;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author slorenzo
 */
public class UDPService {
    
//<editor-fold defaultstate="collapsed" desc="Eventos del servicio">
    
    private List<UDPServiceListener> listeners = new ArrayList<>();
    
    
    public void addUDPServerListener(UDPServiceListener listener) {
        listeners.add(listener);
    }
    
    public void removeUDPServerListener(UDPServiceListener listener) {
        listeners.remove(listener);
    }
    
    public void fireDataReceived(DatagramPacket datagram, InetAddress source, int port, Date dateTime) {
        for(UDPServiceListener listener : this.listeners) {
            listener.dataReceived(datagram, source, port, dateTime);
        }
    }
    
    public void fireDataSended(DatagramPacket datagram, InetAddress source, int port, Date dateTime) {
        for(UDPServiceListener listener : this.listeners) {
            listener.dataSended(datagram, source, port, dateTime);
        }
    }
    
//</editor-fold>
    
    private class UDPPortListener extends Thread {

        @Override
        public void run() {
            try {
                DatagramSocket serverSocket = new DatagramSocket(listenPort); 

                System.out.println(String.format("Esperando por paquetes de datagrama en el puerto UDP %d", listenPort));

                while(running) {

                    receiveData = new byte[sizePackage];

                    DatagramPacket receivePacket = 
                        new DatagramPacket(receiveData, receiveData.length);

                    serverSocket.receive(receivePacket);

                    String sentence = new String(receivePacket.getData());

                    InetAddress IPAddress = receivePacket.getAddress();
                    
                    int port = receivePacket.getPort();

                    fireDataReceived(receivePacket, IPAddress, port, new Date());
                }

            } catch (SocketException ex) {
                System.out.println(String.format("Puerto UDP %d ocupado.", listenPort));
                System.exit(1);
            } catch (IOException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
    private boolean running = true;
    
    private int sizePackage = 1024;
    private int listenPort;
    
    private byte[] receiveData;
    
    private UDPPortListener service = new UDPPortListener();
    
    public UDPService(int port) {
        listenPort = port;
        
        byte[] receiveData = new byte[sizePackage]; 
    }
    
    public UDPService(int port, int size) {
        listenPort = port;
        sizePackage = size;
        
        byte[] receiveData = new byte[sizePackage]; 
    }
    
    public void sendUDP(String data, InetAddress destination, 
                                            int port) throws IOException {
        DatagramSocket socket = null;
        
        try {
            socket = new DatagramSocket();
        } catch (SocketException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        
        byte[] buf = new byte[sizePackage];
        buf = data.toString().getBytes();
        
        DatagramPacket packet = new DatagramPacket(buf, buf.length, destination, port);
        socket.send(packet);
        socket.close();
        
        fireDataSended(packet, destination, port, new Date());
    }
    
    public void sendUDP(final String data, final InetAddress destination, 
                                int port, int timeLapse) throws IOException {
        Timer timer = new Timer();

        timer.schedule(new TimerTask() {
            
            @Override
            public void run() {
                
                try {
                    sendUDP(data, destination, listenPort);
                } catch (IOException ex) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                }
               
            }
            
         }, 0, timeLapse);
    }
    
    public void receive() throws Exception { 
        service.start();
    }
    
    public void stop() {
        service.stop();
    }

}
