/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaddictblog.post3;

import java.util.Locale;

/**
 *
 * @author Soulberto Lorenzo
 */
public class OSHelper {
    
    public static Locale getUserLocale() {
        return new Locale(System.getProperty("user.language"), System.getProperty("user.country"));
    }
    
}
