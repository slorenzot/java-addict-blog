/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaddictblog.post1;

/**
 *
 * @author Soulberto Lorenzo
 */
public class HelloWorld2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] sayHelloTo) {
        for(String person : sayHelloTo) {
            System.out.println(String.format("Hello World, %s!", person));
        }
    }
    
}
