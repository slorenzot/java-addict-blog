package javaddictblog.post2;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author slorenzo
 */
public abstract class UDPServiceListener {
    
    /**
     *
     * @param datagram
     * @param sourceAddress
     * @param port
     * @param dateTime
     */
    public abstract void dataReceived(DatagramPacket datagram, 
                        InetAddress sourceAddress, int port, Date dateTime);
    
    public abstract void dataSended(DatagramPacket datagram, 
                        InetAddress sourceAddress, int port, Date dateTime);
    
}
